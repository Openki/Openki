import { Template } from "./Template";

export const data: Template[] = [
  {
    _id: "L6uD6rsni2fhXvpvT",
    members: ["Support"],
    createdby: "Support",
    short: "QTH",
    name: "Quartiertreff Hirslanden",
    claim: "Leben ins Quartier!",
    description:
      '<p>mehr unter <a href="https://www.qth.ch/" rel="nofollow">https://www.qth.ch/</a> </p>',
    logoUrl: "groups/logos/01b6c66ee5a4e393bb2a04b9.png",
  },
  {
    _id: "MzGp4ZDxcS2uobGcv",
    members: ["Support"],
    createdby: "Support",
    short: "NZ",
    name: "Noch nicht zugeordnet",
    claim: "Vorschläge, die noch nicht einem Austragungsort zugeordnet sind",
    description:
      '<p>Hier trägst du deinen Lektionenvorschlag ein, wenn du noch nicht weisst, in welchem unserer 23 Austragungsorte du deine Lektion durchführen willst. Gleichzeitig bist du flexibel, deine Lektion irgendwo in der Stadt Zürich durchzuführen.</p><p>Wenn du das untenstehende Formular ausfüllst, kommt deine Lektion in unseren "Lektionentopf". Aus diesem können unsere Austragungsorte bei Interesse deine Lektion "fischen" und melden sich dann gleich direkt persönlich bei dir. Dieses Vorgehen kann einige Zeit dauern und wir bitten dich daher um ein wenig Geduld.</p><p>Wenn du schon weisst, wo du deine Lektion durchführen möchtest, gehe zurück auf der <a href="https://zuerich-lernt.ch/schenken/" rel="nofollow">Übersicht unserer 23 Austragungsorte</a> und wähle den Ort in deiner Nähe.</p><p>Wenn du Fragen hast, schreibe uns gerne eine E-Mail an:<br />projekt [at] zuerich-lernt.ch.</p>',
  },
  {
    _id: "LZJm45NouwSuYTpJJ",
    members: ["Support"],
    createdby: "Support",
    short: "Accu",
    name: "Zentrum ELCH Accu",
    claim: "Macht am Montag und am Freitag mit.",
    description: "<p>Zentrum ELCH Accu</p><p>Otto Schütz Weg 9</p><p>8050 Zürich</p>",
  },
  {
    _id: "nakdPuAv53ezjcthr",
    members: ["Support"],
    createdby: "Support",
    short: "Riesba",
    name: "GZ Riesbach",
    claim: "GZ Riesbach,  Seefeldstrasse 93,  8008 Zürich",
    description:
      "<p>Wir machen am Mittwoch bis Freitag bei Zürich lernt mit. Bitte melde uns all deine zeitlichen Möglichkeiten und deine Favoriten, so lässt es sich einfacher planen. </p><p>Trage unten deine Lektion(en)-Idee ein oder komm mit deiner Idee direkt bei uns vorbei! Bei Fragen: ursina.theus@gz-zh.ch oder 044/387 74 50.</p><p>Wir freuen uns auf dein Angebot und ein vielfältiges Lernfestival!<br /></p>",
  },
  {
    _id: "NQK26KFR6Tej6F3uh",
    members: ["Support"],
    createdby: "Support",
    short: "GZ Woho",
    name: "GZ Wollishofen",
    claim: "Macht am Mittwoch und Samstag mit.",
    description: "<p>GZ Wollishofen</p><p>Bachstrasse 7</p><p>8038 Zürich</p>",
    logoUrl: "groups/logos/a89d8786257768f2e1d01cbb.png",
  },
  {
    _id: "vmsMqDCBp929ZvnBN",
    members: ["Support"],
    createdby: "Support",
    short: "Grünau",
    name: "GZ Grünau",
    claim: "Wir sind am Samstag und Sonntag dabei!",
    description:
      '<h4>Die Durchführung </h4><p>der Lektionen ist sowohl am Hauptstandort an der Limmat als auch an unserem zweiten Standort, dem Begegnungsraum im Bundesasylzentrum, möglich. Du kannst uns entweder bereits jetzt mitteilen, wo du <i>deine</i> Lektion durchführen <a href="http://gmx.net" rel="nofollow">möchtest</a> oder wir schauen es mit dir zusammen an, wo es am besten passt.</p><p>Wir machen Montag bis <b>Samstag</b> mit. Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p><p>asdf<br />asdf</p><p>test</p><p><br /></p><ul><li>1</li><li>2</li><li>3</li><li>4</li></ul>',
  },
  {
    _id: "P364gQefC3Rqmy9gC",
    members: ["Support"],
    createdby: "Support",
    short: "Witikon",
    name: "GZ Witikon",
    claim: "Tage noch unklar",
    description: "<p>GZ Witikon</p><p>Witikonerstrasse 405</p><p>8053 Zürich</p>",
  },
  {
    _id: "b5N3to7cQpZgbdgMn",
    members: ["Support"],
    createdby: "Support",
    short: "Leimb",
    name: "GZ Leimbach",
    claim: "GZ Leimbach, Leimbachstrasse 200, 8041 Zürich",
    description:
      "<p>Wir machen am Dienstag, Mittwoch, Freitag und Samstag mit.<br />Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p><p>Wir freuen uns auf dein Angebot und ein vielfältiges Lernfestival!<br /></p>",
  },
  {
    _id: "C82n4psKnd9hN2nbQ",
    members: ["Support"],
    createdby: "Support",
    short: "Loogart",
    name: "GZ Loogarten",
    claim: "Macht am Samstag und Sonntag mit.",
    description: "<p>GZ Loogarten</p><p>Salzweg 1</p><p>8048 Zürich</p>",
  },
  {
    _id: "dpFh6LoroCHLwahBH",
    members: ["Support"],
    createdby: "Support",
    short: "OJA 6 W",
    name: "OJA Kreis 6 & Wipkingen",
    claim: "Macht am Dienstag, Mittwoch, Donnerstag, Freitag, Samstag und Sonntag mit.",
    description: "<p>OJA Kreis 6 &amp; Wipkingen</p><p>Langmauerstrasse 7</p><p>8006 Zürich</p>",
  },
  {
    _id: "BGKaKo49RKgbeAsNt",
    members: ["Support"],
    createdby: "Support",
    short: "QH 6",
    name: "Quartierhaus Kreis 6",
    claim: "Quartierhaus Kreis 6,  Langmauerstrasse 7,  8006 Zürich",
    description:
      "<p>Wir machen Montag bis Sonntag  bei Zürich lernt mit.<br />Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p><p>Wir freuen uns auf dein Angebot und ein vielfältiges Lernfestival!<br /></p>",
  },
  {
    _id: "9tHDDJyKJMgjphiKA",
    members: ["Support"],
    createdby: "Support",
    short: "MeyerhW",
    name: "Meyerhofscheune Witikon",
    claim: "Macht am Dienstag, Mittwoch, Donnerstag, Freitag mit.",
    description: "<p>Meyerhofscheune Witikon</p><p>Witikonerstrasse 396</p><p>8053 Zürich</p>",
  },
  {
    _id: "ZjdbyN3iRCyQTDmTG",
    members: ["Support"],
    createdby: "Support",
    short: "Höngg",
    name: "GZ Höngg",
    claim: "Limmattalstrasse 214 und Hurdäckerstrasse 6 8049 Zürich",
    description:
      "<p>Vielen Dank für die Lektion, die du im Rahmen von Zürich lernt 2023, verschenken möchtest. Wir machen von Donnerstag bis Samstag bei Zürich lernt mit. Das GZ Höngg hat zwei Standorte, die du auswählen kannst: Limmattalstrasse 214 und Hurdäckerstrasse 6 (Standort Rütihof).<br /></p><p>Deinen Vorschlag kannst du unten ins Formular eintragen und wir melden uns innerhalb von zwei Wochen bei dir. Gerne kannst du Ideen und oder fertige Angebote auch über simone@caseri.ch oder telefonisch unter +41 79 236 92 82 anmelden oder besprechen.</p><p>Wir freuen uns auf dein Angebot und ein vielfältiges Lernfestival!<br /></p>",
    logoUrl: "groups/logos/b6daffcc60de2dae9da5d54b.png",
  },
  {
    _id: "oAKa3Es5T8piWAENg",
    members: ["Support"],
    createdby: "Support",
    short: "Hotting",
    name: "GZ Hottingen",
    claim: "GZ Hottingen,  Gemeindestrasse 54,  8032 Zürich",
    description:
      "<p>Wir machen am Mittwoch, Samstag und Sonntag  bei Zürich lernt mit. Nach Absprache sind evtl. weitere Zeitfenster an einem anderen Tag möglich.<br />Trage deinen Vorschlag unten ein, dann nehme ich mit dir Kontakt auf für alle weiteren Details. Du kannst mit deinen Ideen auch direkt bei uns (GZ/Standort Klosbachstrasse) vorbeikommen! Bei Fragen und allen Anliegen: clarina.franziscus@gz-zh.ch oder 044 252 68 14 (nicht erreichbar während der Stadtzürcher Schulferien).</p><p>Wir freuen uns auf dein Angebot und ein vielfältiges Lernfestival!<br /></p>",
  },
  {
    _id: "sSj2doBt5yTTAQEnD",
    members: ["Support"],
    createdby: "Support",
    short: "Schigu",
    name: "GZ Schindlergut",
    claim: "GZ Schindlergut,  Kronenstrasse 12,  8006 Zürich",
    description:
      "<p>Wir machen am Mittwoch und Donnerstag,  bei Zürich lernt mit. Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p><p>Wir freuen uns auf dein Angebot und ein vielfältiges Lernfestival!<br /></p>",
  },
  {
    _id: "9iQhzd2qNpodeiyNb",
    members: ["Support"],
    createdby: "Support",
    short: "ETG",
    name: "Evangelische Täufergemeinde",
    claim: "Macht am Mittwoch, Donnerstag und Samstag, mit.",
    description: "<p>Evangelische Täufergemeinde</p><p>Freiestrasse 83</p><p>8032 Zürich</p>",
  },
  {
    _id: "gfvFDzGkgZrHhiFNS",
    members: ["Support"],
    createdby: "Support",
    short: "Kanzbi",
    name: "Kanzbi - Interkultureller Treffpunkt und Bibliothe",
    claim: "Macht am Montag, Dienstag, Mittwoch, Donnerstag, Freitag mit.",
    description: "<p>Kanzbi</p><p>Kanzleistrasse 56</p><p>8004 Zürich</p>",
  },
  {
    _id: "dmwtkos4TWYJ4pzPs",
    members: ["Support"],
    createdby: "Support",
    short: "GZBa",
    name: "GZ Bachwiesen",
    claim: "GZ Bachwiesen, Bachwiesenstrasse 40, 8047 Zürich",
    description:
      "<p>Wir machen Montag bis Sonntag  bei Zürich lernt mit.</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p><p>Wenn du weitere Fragen hast, kannst du dich gerne an Jasmin (Quartierarbeit GZ Bachwiesen) wenden: jasmin.beurer@gz-zh.ch (Mi-Fr anwesend)</p>",
  },
  {
    _id: "xRu2dnHG97ABFErJo",
    members: ["Support"],
    createdby: "Support",
    short: "GVü",
    name: "Genussvoll - Vielfalt überall",
    claim: "genussvoll.org\n\nc/o Elisa Mosler,  Drusbergstrasse 26,  8053 Zürich",
    description:
      "<p>Der Verein Genussvoll unterstützt auf vielfältige Weise Menschen, die die Welt verbessern wollen.<br /></p><p>Wir machen von Montag bis Sonntag  bei Zürich lernt mit.<br />Trage unten deine Lektions-Idee ein und wir melden uns innerhalb von 2 Wochen bei dir. </p><p>Um etwas vorzuschlagen, darfst du dich auch gerne bei uns auf 078 831 13 47 melden oder uns eine E-Mail schreiben an info@genussvoll.org. Dienstags ist unser Telefon auch bis 20 Uhr besetzt.</p>",
  },
  {
    _id: "rbwTkTcc2JYisNcBn",
    members: ["Support"],
    createdby: "Support",
    short: "Frieden",
    name: "Zentrum ELCH Frieden",
    claim: "Macht am Mittwoch und Donnerstag mit.",
    description: "<p>Zentrum ELCH Frieden</p><p>Wehntalerstrasse 440</p><p>8046 Zürich</p>",
  },
  {
    _id: "k2Mf58cubDZN7Hvhf",
    members: ["Support"],
    createdby: "Support",
    short: "ZE Rbs",
    name: "Zentrum ELCH Regensbergstrasse",
    claim: "Macht am Montag und Freitag mit.",
    description:
      "<p>Zentrum ELCH Regensbergstrasse</p><p>Regensbergstrasse 209</p><p>8050 Zürich</p>",
  },
  {
    _id: "kJSubK8x7CZJnDGkZ",
    members: ["Support"],
    createdby: "Support",
    short: "Kroki",
    name: "Zentrum ELCH Krokodil",
    claim: "Macht am Montag, Dienstag, Mittwoch, Donnerstag, Freitag mit.",
    description: "<p>Zentrum ELCH Krokodil</p><p>Friedrichstrasse 9</p><p>8051 Zürich</p>",
  },
  {
    _id: "3Gvd4PZpoLdM6mvfe",
    members: ["Support"],
    createdby: "Support",
    short: "siGHÖL",
    name: "sichtbar GEHÖRLOSE ZÜRICH",
    claim: "Macht am Dienstag, Donnerstag, Freitag und Samstag mit.",
    description: "<p>sichtbar GEHÖRLOSE ZÜRICH</p><p>Oerlikonerstrasse 98</p><p>8057 Zürich</p>",
  },
  {
    _id: "rscJDvWpSdoFNecXJ",
    members: ["Support"],
    createdby: "Support",
    short: "RefK7&8",
    name: "Ref. Kirche Zürich, Kirchenkreis sieben acht",
    claim: "Macht am Mittwoch mit",
    description:
      "<p>Wir machen am Montag, Mittwoch und Donnerstag bei Zürich lernt mit.<br />Im Neumünster, Kirche Fluntern, sowie Kirchgemeindehaus Hottingen.<br /></p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p>",
  },
  {
    _id: "uaQGXf9HozaQCrob8",
    members: ["Support"],
    createdby: "Support",
    short: "A.us!",
    name: "About Us!",
    claim: "Zürich Interkulturell",
    description:
      '<p><a href="http://www.about-us.ch" rel="nofollow">About Us! </a>Zürich Interkulturell lädt alle 2 Jahre im Sommer in verschiedenen Quartieren zu künstlerischen Aktivitäten und Begegnungen ein. Im Juni 2023 sind wir in Albisrieden, Affoltern und Oerlikon aktiv. Mit Zürich lernt sind wir mit ersten künstlerischen Angeboten in diesen drei Quartieren zu Gast.<br />Wir machen am Dienstag, Mittwoch und Samstag, mit. Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p><p>Wir freuen uns auf dein Angebot und ein vielfältiges Lernfestival!</p>',
    logoUrl: "groups/logos/aa1345bf7e81d54d8fa79026.png",
  },
  {
    _id: "SErB6i2hv8SaWmxF6",
    members: ["Support"],
    createdby: "Support",
    short: "Heuried",
    name: "GZ Heuried",
    claim: "GZ Heuried,  Döltschiweg 130,  8055 Zürich",
    description:
      "<p>Wir machen am Mittwoch und Donnerstag bei Zürich lernt mit.</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p>",
  },
  {
    _id: "yJBGGe8K4Pb9ZLEoX",
    members: ["Support"],
    createdby: "Support",
    short: "Schütze",
    name: "Quartierzentrum Schütze",
    claim: "Heinrichstrasse 238, 8005 Zürich",
    description:
      "<p>Wir machen Montag, Mittwoch und Donnerstag mit.<br /></p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p><p></p><p>Wir freuen uns auf dein Angebot und ein vielfältiges Lernfestival!</p>",
  },
  {
    _id: "QeppkGZxMmihjbhZf",
    members: ["Support"],
    createdby: "Support",
    short: "QTWG",
    name: "Quartiertreff Waldgarten",
    claim: "Quartiertreff Waldgarten,  Regensbergstrasse 35,  8050 Zürich",
    description:
      "<p>Im Quartiertreff Waldgarten finden während der ganzen Woche Lektionen  statt.<br />Die Details sind mit den verantwortlichen Personen abzusprechen.<br /></p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p>",
  },
  {
    _id: "fHJLoDnPmqm2BhzeF",
    members: ["Support"],
    createdby: "Support",
    short: "GZW-BAZ",
    name: "GZ Wipkingen",
    claim: "Macht am Montag, Mittwoch und Donnerstag mit.",
    description:
      "<p>GZ Wipkingen <br /></p><p>Breitensteinstrasse 19a</p><p>8037 Zürich</p><p><br /></p><p></p><p>GZ Wipkingen, Standort BAZ<br /></p><p>Duttweilerstrasse 11</p><p>8005 Zürich</p>",
  },
  {
    _id: "dxQTANZsF6koGxqCW",
    members: ["Support"],
    createdby: "Support",
    short: "PBZ Bib",
    name: "PBZ Bibliothek Hardau",
    claim: "Norastrasse 20, 8004 Zürich",
    description:
      "<p>Die PBZ Hardau macht von Dienstag bis Samstag bei Zürich lernt mit. Trage unten deine Lektions-Idee ein und wir melden uns bei dir.  Gerne kannst du Ideen oder fertige Angebote auch über christina.villiger@pbz.ch sowie telefonisch unter +41 44 204 98 40 anmelden oder besprechen. <br /></p><p>Wir freuen uns auf dein Angebot und ein vielfältiges Lernfestival!<br /></p>",
  },
  {
    _id: "Z2xv3qJZM4aoLBdes",
    members: ["Support"],
    createdby: "Support",
    short: "Sonnegg",
    name: "Sonnegg Familien- und Generationenhaus Höngg",
    claim: "Macht Montag bis Sonntag mit",
    description:
      "<p>Familien- und Generationenhaus Sonnegg</p><p>Bauherrenstrasse 53</p><p>8049 Zürich</p>",
  },
  {
    _id: "RXTtSroQuGbaJJdEP",
    members: ["Support"],
    createdby: "Support",
    short: "Openki",
    name: "Openki",
    claim: "Kurzfristige Lektionen",
    description: "<p>Openki</p><p>Sihlquai 131</p><p>8005 Zürich</p>",
  },
  {
    _id: "BwNrZaW7dqSgv7BjH",
    members: ["Support"],
    createdby: "Support",
    short: "TBC",
    name: "To be completed",
    claim: "Fehlen noch infos",
    description:
      "<p>Es fehlt etwas </p><p>- die Gruppen</p><p>- Beschreibungstext</p><p>- Ort beim Event</p><p>- von Feld</p><p>- für Feld</p><p><br /></p><p>ggf später auch fehlender event</p>",
  },
  {
    _id: "buwCFod8Qsnin9c5o",
    members: ["Support"],
    createdby: "Support",
    short: "np",
    name: "Nicht im Programmheft",
    claim: "Für alle Lektionen, die nicht im Programmheft abgedruckt sind",
    description:
      "<p>Dieses Label ist für Lektionen nach der Programmheftdeadline vorgeschlagen oder festgelegt wurden.</p>",
  },
  {
    _id: "EJ7aqYqPQk4zGtKPc",
    members: ["Support"],
    createdby: "Support",
    short: "Test",
    name: "Test AO 23",
    claim: "Zum Testen für Kommunikaiton",
    description: "<p>Von UX Abläuffen und Benachrichtigungen und Mails<br /></p>",
  },
  {
    _id: "mfBpRGdW5LDCcEPk2",
    members: ["Support"],
    createdby: "Support",
    short: "ASZ",
    name: "Autonome Schule Zürich",
    claim: "Macht am Dienstag, Donnerstag und Samstag mit.",
    description: "<p>Autonome Schule, Sihlquai 125</p><p>Sihlquai 125</p><p>8005 Zürich</p>",
  },
  {
    _id: "BMvkjFghun6qW4M9K",
    members: ["Support"],
    createdby: "Support",
    short: "RefWiti",
    name: "Reformierte Kirche Witikon",
    claim: "Reformierte Kirche Witikon,  Witikonerstrasse 286,  8053 Zürich",
    description:
      "<p>Wir machen vom Montag bis Sonntag  bei Zürich lernt mit.</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p>",
    logoUrl: "groups/logos/7a1066f9ea99dd9baf441ebb.jpeg",
  },
  {
    _id: "bQKfskH5pz8Di4hGa",
    members: ["Support"],
    createdby: "Support",
    short: "K-Park",
    name: "Kulturpark",
    claim: "Macht am Montag, Dienstag und Mittwoch mit.",
    description: "<p>Kulturpark</p><p>Pfingstweidstrasse 16</p><p>8005 Zürich</p>",
  },
  {
    _id: "SqaEk7NCrtEZMLKsb",
    members: ["Support"],
    createdby: "Support",
    short: "PBZAlsn",
    name: "PBZ Bibliothek Altstetten",
    claim: "Macht Montag bis und mit Samstag mit",
    description: "<p>PBZ Bibliothek Altstetten</p><p>Lindenplatz 4</p><p>8048 Zürich</p>",
  },
  {
    _id: "3rLCvz8TFLCwwcreF",
    members: ["Support"],
    createdby: "Support",
    short: "PBZ AL",
    name: "PBZ Bibliothek Altstadt",
    claim: "Pestalotzi Bibliothek Altstadt,  Zähringerstrasse 17,  8001 Zürich",
    description:
      "<p>Wir machen am Montag bis Samstag bei Zürich lernt mit.<br /></p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p>",
  },
  {
    _id: "LqJsgumRgmLyqCQwo",
    members: ["Support"],
    createdby: "Support",
    short: "Lernstb",
    name: "Lernstube Oerlikon",
    claim: "Macht am Samstag mit.",
    description: "<p>Lernstube Oerlikon</p><p>Regensbergstrasse 209</p><p>8050 Zürich</p>",
  },
  {
    _id: "3PhDJ6Gk6i9qQdAXJ",
    members: ["Support"],
    createdby: "Support",
    short: "Ws-Akad",
    name: "Partizipative Wissenschaftsakademie",
    claim: "Macht am Montag, Dienstag, Mittwoch und Donnerstag mit.",
    description:
      "<p>Partizipative Wissenschaftsakademie</p><p>Kurvenstrasse 17</p><p>8006 Zürich</p>",
  },
  {
    _id: "CsePhF4b7du8yAhaS",
    members: ["Support"],
    createdby: "Support",
    short: "AStHaus",
    name: "Altstadthaus",
    claim: "Mitten in Zürich",
    description: "<p>Altstadthaus</p><p>Obmannamtsgasse 15</p><p>8001 Zürich</p>",
  },
  {
    _id: "zTkLPCGyxvadeLKG6",
    members: ["Support"],
    createdby: "Support",
    short: "LeLabor",
    name: "LeLabor",
    claim: "LeLabor,  Limmatstrasse 25,  8005 Zürich",
    description:
      "<p>Wir machen am Montag, Mittwoch und Freitag bei Zürich lernt mit.</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p>",
  },
  {
    _id: "5MiKEfwC8fyQLuPkp",
    members: ["Support"],
    createdby: "Support",
    short: "Intrins",
    name: "Intrinsic",
    claim: "Zahnradstr. 22, 8005 Zürich",
    description:
      "<p>Wir machen von Montag bis Sonntag  bei Zürich lernt mit.</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p>",
  },
  {
    _id: "fhupZhFxFCD7FwBSm",
    members: ["Support"],
    createdby: "Support",
    short: "Wunsch",
    name: "Wünsche für Lektionen",
    claim: "Hier werden Wunschlektionen gesammelt",
    description:
      "<p>Alle können sich dem Wunsch anschliessen indem sie ihr Interesse daran melden.</p><p>Falls du die Fähigkeit besitzt, eine Lektion dazu zu schenken, kannst du dich als Schenkende eintragen.</p>",
    customCourseFields: [
      {
        name: "wisher",
        displayText: {
          en: "Wishing person Contact (not public)",
          de: "Wünschende*r Kontakt (nicht öffentlich)",
        },
        editText: {
          en: "Wishing person (not public)",
          de: "Wünschende*r: Dein Name und Kontanktangaben (nicht öffentlich sichtbar)",
        },
        editPlaceholder: {
          en: "Sophia Suliman, 076 543 21 00, sophia@example.com",
          de: "Maria Muster, 076 543 21 00, maria@example.com",
        },
        visibleFor: "editors",
      },
    ],
  },
  {
    _id: "Hx7iiRX5Fg3Pf2xne",
    members: ["Support"],
    createdby: "Support",
    short: "ZL24",
    name: "Zürich lernt 2024",
    claim: "Wissen schenken.",
    description:
      '<p>Mehr Wissenswertes über uns gibt es auf unserer Homepage: <a href="http://zuerich-lernt.ch" rel="nofollow">zuerich-lernt.ch</a>. </p>',
    logoUrl: "groups/logos/b19b1c55c3c3671fad0347d4.png",
    customCourseFields: [
      {
        name: "instructor",
        displayText: { en: "Instructor", de: "Leitung" },
        editText: { en: "Instructor", de: "Leiter:in" },
        editPlaceholder: { en: "Sophia Sand", de: "Maria Muster" },
        visibleFor: "all",
      },
      {
        name: "target",
        displayText: { en: "For", de: "Für" },
        editText: { en: "Target audience", de: "Zielgruppe" },
        editPlaceholder: {
          en: "eg. Childs, Adults",
          de: 'z.B. "Alle", "Ab 12 Jahren" oder "Volksschulkinder in Begleitung eines Erwachsenen"',
        },
        visibleFor: "all",
      },
      {
        name: "phone",
        displayText: { en: "Phone number (not public)", de: "Telefonnummer (nicht öffentlich)" },
        editText: {
          en: "Phone number (will not be published)",
          de: "Telefonnummer (wird nicht veröffentlicht)",
        },
        editPlaceholder: "+41 ## ### ## ##",
        visibleFor: "editors",
      },
      {
        name: "email",
        displayText: { en: "Email address (not public)", de: "E-Mail Adresse (nicht öffentlich)" },
        editText: {
          en: "Email address (will not be published)",
          de: "E-Mail Adresse (wird nicht veröffentlicht)",
        },
        editPlaceholder: "dein.name@example.com",
        visibleFor: "editors",
      },
      {
        name: "orga",
        displayText: { en: "Orga Notes (not public)", de: "Organisatorisches (nicht öffentlich)" },
        editText: {
          en: "Organisational Notes (Visible for donor and Venue only, optional)",
          de: "Organisatorische Mitteilungen (Sichtbar für Schenker:in und Austragungsort, optional)",
        },
        editPlaceholder: {
          en: "I need a projector and only free on Wednesday",
          de: "Gibt es einen Beamer? Ich kann nur am Mittwoch Abend",
        },
        visibleFor: "editors",
        type: "multiLine",
      },
    ],
  },
  {
    _id: "42bNNenBx2Jbobm9r",
    members: ["Support"],
    createdby: "Support",
    short: "MAXIM",
    name: "MAXIM Theater",
    claim: "Ernastrasse 20, 8004\tZürich",
    description:
      "<p>Wir machen vom Mittwoch bis Samstag mit.</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p>",
  },
  {
    _id: "Qxdoq84bqbwG3qvZk",
    members: ["Support"],
    createdby: "Support",
    short: "PBZ SZ",
    name: "PBZ Bibliothek Schütze",
    claim: "PBZ Bibliothek Schütze, Heinrichstrasse 238, 8005 Zürich",
    description:
      "<p>Wir machen von Montag bis Samstag bei Zürich lernt mit.</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir. </p><p>Wir sind auch daran interessiert, Veranstaltungen für Kinder zu organisieren.</p>",
  },
  {
    _id: "2RYY82cFigyLs6etP",
    members: ["Support"],
    createdby: "Support",
    short: "Zumstn",
    name: "Papeterie Zumstein",
    claim: "Papeterie Zumstein, Rennweg 19, 8001 Zürich",
    description:
      "<p>Wir machen am Montag, Mittwoch, Donnerstag und Freitag bei Zürich lernt mit.</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir. Wir möchten uns auf Mal- und Zeichenkurse fokussieren.</p>",
  },
  {
    _id: "6rSMTsSmXAj9rdPF6",
    members: ["Support"],
    createdby: "Support",
    short: "BSLgr",
    name: "Basislager",
    claim: "BASISLAGER, Aargauerstrasse 52-94, 8048 Zürich",
    description:
      "<p>Wir machen am Freitag und Samstag bei Zürich lernt mit.</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p>",
  },
  {
    _id: "F9aByF9suBzKEw5Wp",
    members: ["Support"],
    createdby: "Support",
    short: "St Oase",
    name: "Stadtoase VGZ",
    claim: "Stadtoase VGZ, Tobelhofstrasse 21, 8044 Zürich",
    description:
      "<p>Wir machen Montag und Dienstag bei Zürich lernt mit.</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir.</p>",
  },
  {
    _id: "Y6N5ymAtT8iH9NG2B",
    members: ["Support"],
    createdby: "Support",
    short: "FOGO",
    name: "FOGO Leben am Vulkanplatz",
    claim: "FOGO, Geerenweg 12, 8048 Zürich",
    description:
      "<p>Schön, dass du eine Lektion auf dem FOGO schenken möchtest! Das Zürich lernt findet bei uns an folgenden Tagen statt: </p><p>- Freitag, 20.9., von 16:00 - 21:00 Uhr</p><p>- Samstag, 21.9., von 13:00 - 16:00 Uhr</p><p>Trage unten deine Lektions-Idee ein und wir melden uns bei dir. Wir freuen uns von dir zu hören! </p><p><br /></p><p>Bei Fragen kannst du uns auch gerne kontaktieren: engagement@aoz.ch oder 044 415 60 87 (Di - Fr)</p>",
  },
  {
    _id: "eezae9HKo2XrLeBNe",
    members: ["Support"],
    createdby: "Support",
    short: "MQ",
    name: "Mein Quartier",
    claim: "Die Quartierplattform\nder Stadt Zürich",
    description:
      '<p>Vorschläge, die über <a href="https://meinquartier.zuerich/assemblies/zuerich-lernt/f/96/" rel="nofollow">https://meinquartier.zuerich/assemblies/zuerich-lernt/f/96/</a> vorgeschlagen wurden</p>',
  },
  {
    _id: "ZvdQHfvQbpEwz8ukx",
    members: ["Support"],
    createdby: "Support",
    short: "Heuried",
    name: "GZ Heuried",
    claim: "Zürich lernt",
    description: "<p>Zürich lernt</p>",
  },
  {
    _id: "Po76yKANXHTbz5QBa",
    members: ["Support"],
    createdby: "Support",
    short: "📗",
    name: "Im Programmheft",
    claim: "Diese Lektion ist im Programmheft",
    description:
      "<p>Die Lektionen mit diesem Symbol sind im gedruckten Programmheft zu finden<br /></p>",
  },
  {
    _id: "LAFmoMPwRxguepHiF",
    members: ["Support"],
    createdby: "Support",
    short: "ZH",
    name: "Zürich",
    claim: "Zürich lernt",
    description: "<p>Hochsensibilität - Ernährung - Ajurveda</p>",
  },
];
