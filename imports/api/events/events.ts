import { Mongo } from "meteor/mongo";
import { Meteor } from "meteor/meteor";
import { _ } from "meteor/underscore";
import { Match, check } from "meteor/check";
// eslint-disable-next-line import/no-cycle
import * as tenantDenormalizer from "./tenantDenormalizer";

import { Geodata } from "/imports/api/regions/regions";
import { UserModel } from "/imports/api/users/users";

import { Filtering } from "/imports/utils/filtering";
import { LocalTime } from "/imports/utils/local-time";
import * as Predicates from "/imports/utils/predicates";
import { escapeRegExp, intersection } from "lodash";
import * as FileStorage from "/imports/utils/FileStorage";
import { PublicSettings } from "/imports/utils/PublicSettings";
import { FieldSort, FieldSortPattern, Sort } from "/imports/utils/sort-spec";
import { Tuple, Type } from "/imports/utils/CustomChecks";

export interface EventParticipantEntity {
  user: string;
  companions?: number;
}

export interface EventVenueEntity {
  /**
   * Optional reference to a document in the Venues collection. If this is set, the fields
   * name, loc, and address are synchronized
   */
  _id?: string;
  /** Descriptive name for the venue */
  name: string;
  /** Event location in GeoJSON format */
  loc?: Geodata;
  /** Address string where the event will take place */
  address?: string;
  editor?: string;
}

/** DB-Model */
export interface EventEntity {
  /** ID */
  _id: string;
  /** tenant ID */
  tenant: string;
  /** ID_region */
  region: string;
  title: string;
  slug: string;
  description: string;
  /** (calculated) categories from the course */
  categories?: string[];
  /** String of local date when event starts */
  startLocal: string;
  /** String of local date when event ends */
  endLocal: string;
  venue?: EventVenueEntity;
  /** (Where inside the building the event will take place) */
  room: string;
  /** userId */
  createdBy: string;
  // eslint-disable-next-line camelcase
  time_created: Date;
  // eslint-disable-next-line camelcase
  time_lastedit: Date;
  /** course._id of parent course, optional */
  courseId?: string;
  /** (Events are only displayed when group or venue-filter is active) */
  internal: boolean;
  /** Send a reminder to all attendees of an event just before the event starts */
  sendReminder: boolean;
  /** Shows the event as canceled. */
  canceled?: boolean;
  /** list of group._id that promote this event ("promote groups"). */
  groups: string[];
  /**
   * list of group._id that are allowed to edit the course ("team groups", based on the ui design:
   * Every "team group" promotes the event and is part of the groups list). */
  groupOrganizers: string[];
  /** ID of the replication parent, only cloned events have this */
  replicaOf?: string;
  participants?: EventParticipantEntity[];
  /** Registration is disabled */
  noRsvp: boolean;
  /** maximum participants of event, 0 means no participant limit */
  maxParticipants: number;
  /**
   * (calculated) list of group._id inherited from course (if courseId is set) ("promote groups"
   * from course)
   */
  courseGroups: string[];
  /**
   * (calculated) all groups that promote this course, both inherited from course and set on the
   * event itself ("promote groups" from event and course)
   */
  allGroups?: string[];
  /**
   * (calculated) list of user and group _id that are allowed to edit the event, calculated from
   * the groupOranizers from the event and the editors from the course
   */
  editors: string[];
  /**
   * (calculated) date object calculated from startLocal field. Use this for ordering between
   * events.
   */
  start: Date;
  /** (calculated) date object calculated from endLocal field. */
  end: Date;
  /** (calculated) compact image from the course, scaled down to max size (width*height) 500px */
  courseCompactImage: string;
  /** (calculated) details image from the course, scaled down to max size (width*height) 1500px */
  courseImage: string;
}

export type EventModel = OEvent & EventEntity;

// Event is a built-in, so we use a different name for this class
export class OEvent {
  editors: string[] = [];

  isPrivate(this: EventModel) {
    return !PublicSettings.publicTenants.includes(this.tenant);
  }

  editableBy(this: EventModel, user: UserModel | null | undefined) {
    if (!user) {
      return false;
    }
    if (user.privileged("admin")) {
      return true;
    }
    return intersection(user.badges, this.editors).length > 0;
  }

  sameTime(
    this: { startLocal: string; endLocal: string },
    event: { startLocal: string; endLocal: string },
  ) {
    return (["startLocal", "endLocal"] as const).every((time) => {
      const timeA = LocalTime.fromString(this[time]);
      const timeB = LocalTime.fromString(event[time]);

      return timeA.hour() === timeB.hour() && timeA.minute() === timeB.minute();
    });
  }

  publicUrlForCompactImage(this: EventModel) {
    if (!this.courseCompactImage) {
      return undefined;
    }

    return FileStorage.generatePublicUrl(this.courseCompactImage);
  }

  publicUrlForDetailsImage(this: EventModel) {
    if (!this.courseImage) {
      return undefined;
    }

    return FileStorage.generatePublicUrl(this.courseImage);
  }

  attendedBy(this: EventModel, userId: string | null | undefined) {
    if (!userId) {
      return false;
    }
    return this.participants?.some((p) => p.user === userId) || false;
  }

  numberOfParticipants(this: Pick<EventModel, "participants">) {
    return this.participants?.map((p) => (p.companions || 0) + 1).reduce((a, b) => a + b, 0) || 0;
  }

  isFullyBooked(this: EventModel) {
    if (this.noRsvp) {
      return false;
    }
    if (!this.maxParticipants) {
      return false;
    }
    return this.numberOfParticipants() >= this.maxParticipants;
  }
}

export const FindFilterPattern = {
  /** string of words to search for */
  search: Match.Maybe(String),
  /** include only events that overlap the given period (list of start and end date) */
  period: Match.Maybe(Tuple<[start: DateConstructor, end: DateConstructor]>(Date, Date)),
  /** only events that end after this date (so it also includes ongoning events at this point in time) */
  start: Match.Maybe(Date),
  /**  only events starting after this date */
  after: Match.Maybe(Date),
  /** only events that are ongoing during this date */
  ongoing: Match.Maybe(Date),
  /**
   * only events that ended before this date, if none "after" is given this also changes the
   * order of events
   */
  before: Match.Maybe(Date),
  /** only events that started before this date, (so it also includes ongoning events at this point in time) */
  end: Match.Maybe(Date),
  /** only events at this venue (ID) */
  venue: Match.Maybe(String),
  /** only events at this venues (IDs) */
  venues: Match.Maybe([String]),
  /** only events in this room (string match) */
  room: Match.Maybe(String),
  /** only events that are not attached to a course */
  standalone: Match.Maybe(Boolean),
  /** restrict to given region */
  region: Match.Maybe(String),
  /** restrict to given tenants */
  tenants: Match.Maybe([String]),
  /** list of category ID the event must be in */
  categories: Match.Maybe([String]),
  /** the event must be in that group (ID) */
  group: Match.Maybe(String),
  /** the event must be in one of the group ID */
  groups: Match.Maybe([String]),
  /** the event must be in none of the group ID */
  notGroup: Match.Maybe(String),
  /** the event must be in none of the group ID */
  notGroups: Match.Maybe([String]),
  /** only events for this course (ID) */
  course: Match.Maybe(String),
  /** only events that are internal (if true) or public (if false), Default: Show both */
  internal: Match.Maybe(Boolean),
  /** only events that are canceled (if true) or not canceled (if false), Default: Show both */
  canceled: Match.Maybe(Boolean),
  /** only events with given participant (ID) */
  participant: Match.Maybe(String),
};
export type FindFilter = Type<typeof FindFilterPattern>;

export const FindParams = {
  course: Predicates.id,
  region: Predicates.id,
  participant: Predicates.id,
  search: Predicates.string,
  categories: Predicates.ids,
  group: Predicates.id,
  groups: Predicates.ids,
  notGroup: Predicates.id,
  notGroups: Predicates.ids,
  venue: Predicates.id,
  venues: Predicates.ids,
  room: Predicates.string,
  start: Predicates.date,
  after: Predicates.date,
  before: Predicates.date,
  end: Predicates.date,
  internal: Predicates.flag,
  canceled: Predicates.flag,
};

export class EventsCollection extends Mongo.Collection<EventEntity, EventModel> {
  constructor() {
    super("Events", {
      transform(event) {
        return _.extend(new OEvent(), event);
      },
    });

    if (Meteor.isServer) {
      this.createIndex({ tenant: 1, region: 1, start: 1 });
      this.createIndex({ tenant: 1, region: 1, end: 1 });
      this.createIndex({ tenant: 1, region: 1, "venue._id": 1 });
      this.createIndex({ tenant: 1, region: 1, allGroups: 1 });
    }
  }

  async insertAsync(
    event: Omit<Mongo.OptionalId<EventEntity>, "tenant">,
    callback?: (err?: unknown, _id?: string) => void,
  ) {
    const enrichedEvent = await tenantDenormalizer.beforeInsert(event);

    return super.insertAsync(enrichedEvent, callback);
  }

  // eslint-disable-next-line class-methods-use-this
  Filtering() {
    return new Filtering(FindParams);
  }

  /**
   * Find events for given filters
   * @param filter dictionary with filter options
   * @param limit how many to find
   * @param skip skip this many before returning results
   * @param sort list of fields to sort by
   *
   * The events are sorted by start date (ascending, before-filter causes descending order)
   */
  findFilter(filter: FindFilter = {}, limit = 0, skip = 0, sort: FieldSort[] = []) {
    check(filter, Match.Maybe(FindFilterPattern));
    check(limit, Match.Maybe(Match.Integer));
    check(skip, Match.Maybe(Match.Integer));
    check(sort, Match.Maybe([FieldSortPattern]));

    const find: any = {};
    const and = [];

    const options: Mongo.Options<EventEntity> = {};

    let startSortOrder: Sort = "asc";

    if (limit > 0) {
      options.limit = limit;
    }

    if (skip > 0) {
      options.skip = skip;
    }

    if (filter.period) {
      find.start = { $lt: filter.period[1] }; // Start date before end of period
      find.end = { $gte: filter.period[0] }; // End date after start of period
    }

    if (filter.start) {
      and.push({ end: { $gte: filter.start } });
    }

    if (filter.end) {
      and.push({ start: { $lte: filter.end } });
    }

    if (filter.after) {
      find.start = { $gt: filter.after };
    }

    if (filter.ongoing) {
      find.start = { $lte: filter.ongoing };
      find.end = { $gte: filter.ongoing };
    }

    if (filter.before) {
      find.end = { $lt: filter.before };
      if (!filter.after) {
        startSortOrder = "desc";
      }
    }

    let inVenues = [];
    if (filter.venue) {
      inVenues.push(filter.venue);
    }

    if (filter.venues) {
      inVenues = inVenues.concat(filter.venues);
    }

    if (inVenues.length > 0) {
      find["venue._id"] = { $in: inVenues };
    }

    if (filter.room) {
      find.room = filter.room;
    }

    if (filter.standalone) {
      find.courseId = { $exists: false };
    }

    if (filter.tenants && filter.tenants.length > 0) {
      find.tenant = { $in: filter.tenants };
    }

    if (filter.region) {
      find.region = filter.region;
    }

    if (filter.categories) {
      find.categories = { $all: filter.categories };
    }

    let inGroups = [];
    if (filter.group) {
      inGroups.push(filter.group);
    }

    if (filter.groups) {
      inGroups = inGroups.concat(filter.groups);
    }

    if (inGroups.length > 0) {
      find.allGroups = { $in: inGroups };
    }

    let notInGroups = [];
    if (filter.notGroup) {
      notInGroups.push(filter.notGroup);
    }

    if (filter.notGroups) {
      notInGroups = notInGroups.concat(filter.notGroups);
    }

    if (notInGroups.length > 0) {
      find.allGroups = { $nin: notInGroups };
    }

    if (filter.course) {
      find.courseId = filter.course;
    }

    if (filter.participant) {
      find["participants.user"] = filter.participant;
    }

    if (filter.internal !== undefined) {
      find.internal = Boolean(filter.internal);
    }

    if (filter.canceled !== undefined) {
      if (filter.canceled) {
        find.canceled = { $eq: true };
      } else {
        find.canceled = { $ne: true };
      }
    }

    if (filter.search) {
      const searchTerms = filter.search.split(/\s+/);
      searchTerms.forEach((searchTerm) => {
        and.push({
          $or: [
            { title: { $regex: escapeRegExp(searchTerm), $options: "i" } },
            { description: { $regex: escapeRegExp(searchTerm), $options: "i" } },
          ],
        });
      });
    }

    if (and.length > 0) {
      find.$and = and;
    }

    sort.push(["start", startSortOrder]);

    options.sort = sort;

    return this.find(find, options) as Mongo.Cursor<EventEntity, EventModel>;
  }
}

export const Events = new EventsCollection();
