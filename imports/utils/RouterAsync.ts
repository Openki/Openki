import { Router } from "meteor/iron:router";
import { ReactiveVar } from "meteor/reactive-var";

import { SubscriptionHandle } from "/imports/utils/ServerPublishBlaze";

// Extends the route function to support async await in onRun

export function route<D, S extends Record<string, SubscriptionHandle<any>> | undefined>(
  pathOrRouteName: string,
  options: Omit<Router.RouteOptions<D>, "data"> & {
    onRun?: () => Promise<unknown>;
    subscriptions: (this: Router.RouteThis) => S;
    data?: (
      this: Router.RouteThis & {
        subscriptions: (this: Router.RouteThis) => S;
      },
    ) => D;
  },
): void;
export function route<D>(
  pathOrRouteName: string,
  options: Omit<Router.RouteOptions<D>, "data"> & {
    onRun?: () => Promise<unknown>;
  },
): void;
export function route<D>(
  pathOrRouteName: string,
  options: Omit<Router.RouteOptions<D>, "data"> & {
    onRun?: () => Promise<unknown>;
    data: (this: Router.RouteThis) => D;
  },
): void;
export function route<D, S extends Record<string, SubscriptionHandle<any>> | undefined>(
  pathOrRouteName: string,
  options: Omit<Router.RouteOptions<D>, "data"> & {
    onRun?: () => Promise<unknown>;
    subscriptions?: (this: Router.RouteThis) => S;
    data?: (
      this: Router.RouteThis & {
        subscriptions: (this: Router.RouteThis) => S;
      },
    ) => D;
  },
) {
  const subscriptions = options.subscriptions;
  // eslint-disable-next-line no-param-reassign
  delete options.subscriptions;

  if (subscriptions && options.waitOn) {
    throw new Error("not supported");
  }

  if (subscriptions) {
    const data = options.data;
    let handlers: NonNullable<S>;

    // load data, remove async call, keep reactive
    let result: D;
    let currentRequestId = "";
    const dataLoaded = new ReactiveVar(false);
    let dataLoading = false;

    if (data) {
      // eslint-disable-next-line no-param-reassign
      options.data = function () {
        this.subscriptions = () => handlers;
        try {
          if (
            !dataLoading &&
            !dataLoaded.get() &&
            Object.values(handlers).every((s) => !s.isLoading())
          ) {
            dataLoading = true;
            (async () => data?.call(this))()
              .then((r) => {
                result = r;
              })
              .finally(() => {
                dataLoading = false;
                dataLoaded.set(true);
              });
          }
          return result;
        } finally {
          delete (this as any).subscriptions;
        }
      };
    } else {
      dataLoaded.set(true);
    }

    // eslint-disable-next-line no-param-reassign
    options.waitOn = function () {
      // prevent loading multiple times the same data;
      handlers = subscriptions.call(this);
      if (currentRequestId !== JSON.stringify(Object.entries(this.params))) {
        currentRequestId = JSON.stringify(Object.entries(this.params));

        // reset state of loading
        dataLoading = false;
        if (data) {
          dataLoaded.set(false);
        } else {
          dataLoaded.set(true);
        }
      }

      return Object.values(handlers).map((s) => () => {
        return !s.isLoading() && dataLoaded.get();
      });
    };
  }

  if (!options.onRun) {
    Router.route(pathOrRouteName, options as any);
    return;
  }

  const onRunOption = options.onRun;
  // eslint-disable-next-line no-param-reassign
  delete options.onRun;

  const waitOn = options.waitOn;
  // eslint-disable-next-line no-param-reassign
  delete options.waitOn;

  const onRunAwaited = new ReactiveVar<boolean>(false);
  const newOptions = {
    onRun() {
      onRunOption?.().then(() => {
        onRunAwaited.set(true);
      });
      (this as any).next();
    },
    waitOn: waitOn
      ? function () {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          const waitOnResult = waitOn.call(this);

          if (Array.isArray(waitOnResult)) {
            return [
              ...waitOnResult,
              function () {
                return onRunAwaited.get();
              },
            ];
          }

          return [
            waitOnResult,
            function () {
              return onRunAwaited.get();
            },
          ];
        }
      : function () {
          return function () {
            return onRunAwaited.get();
          };
        },
    ...options,
  } as Router.RouteOptions<D>;

  Router.route(pathOrRouteName, newOptions);
}
