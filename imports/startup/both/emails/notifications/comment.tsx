import React from "react";
import { check } from "meteor/check";
import { Router } from "meteor/iron:router";
import { Meteor } from "meteor/meteor";
import { truncate, uniq } from "lodash";
import i18next from "i18next";

import { CourseDiscussions } from "/imports/api/course-discussions/course-discussions";
import { CourseModel, Courses } from "/imports/api/courses/courses";
import { RegionModel, Regions } from "/imports/api/regions/regions";
import { UserModel, Users } from "/imports/api/users/users";
import { Log } from "/imports/api/log/log";

import { getSiteName } from "/imports/utils/getSiteName";
import { Model as NotificationModel } from "../notification";
import { Comment, Props } from "/imports/emails/notifications/Comment";

export interface Body {
  commentId: string;
  recipients: string[];
  model: string;
}

/**
 * Record the intent to send event notifications
 * @param commentId ID for the CourseDiscussions collection
 */
export async function record(commentId: string) {
  check(commentId, String);
  const comment = await CourseDiscussions.findOneAsync(commentId);
  if (!comment) {
    throw new Meteor.Error(`No CourseDiscussion entry for ${commentId}`);
  }

  const course = await Courses.findOneAsync(comment.courseId);
  if (!course) {
    throw new Meteor.Error(`No course entry for ${commentId}`);
  }

  let recipients;
  if (comment.notifyAll) {
    recipients = course.members.map((m) => m.user);
  } else {
    recipients = [];

    recipients = course.membersWithRole("team").map((m) => m.user);

    // All participants in the thread are notified.
    const threadId = comment.parentId;
    if (threadId) {
      const threadSelector = {
        $or: [{ _id: threadId }, { parentId: threadId }],
      };

      await CourseDiscussions.find(threadSelector).forEachAsync((threadComment) => {
        const partId = threadComment.userId;
        if (partId) {
          recipients.push(partId);
        }
      });
    }

    // Don't send to author of comment
    if (comment.userId) {
      recipients = recipients.filter((r) => r !== comment.userId);
    }

    recipients = uniq(recipients);
  }

  const body: Body = {
    commentId: comment._id,
    recipients,
    model: "Comment",
  };

  await Log.record("Notification.Send", [course._id, comment._id], body);
}

export async function Model(entry: { body: Body }): Promise<NotificationModel<Props>> {
  const comment = await CourseDiscussions.findOneAsync(entry.body.commentId);

  let course: CourseModel | undefined;
  let commenter: UserModel | undefined;
  let commenterName: string | undefined;

  if (comment) {
    course = await Courses.findOneAsync(comment.courseId);
    if (comment.userId) {
      commenter = await Users.findOneAsync(comment.userId);
    }
    if (commenter) {
      commenterName = commenter.getDisplayName();
    }
  }

  return {
    accepted(actualRecipient) {
      if (actualRecipient.notifications === false) {
        throw new Error("User wishes to not receive automated notifications");
      }
    },

    async vars(locale, _actualRecipient, unsubToken) {
      const t = i18next.getFixedT(locale);
      const campaign = "commentNotify";

      if (!comment) {
        throw new Error("Comment does not exist (0.o)");
      }
      if (!course) {
        throw new Error("Course does not exist (0.o)");
      }

      const subjectvars = {
        COURSE: truncate(course.name, { length: 10 }),
        TITLE: truncate(comment.title, { length: 50 }),
        COMMENTER: "",
      };

      let subject;
      if (commenter) {
        subjectvars.COMMENTER = truncate(commenterName, { length: 20 });
        subject = t(
          "notification.comment.mail.subject",
          "Comment on {COURSE} by {COMMENTER}: {TITLE}",
          subjectvars,
        );
      } else {
        subject = t(
          "notification.comment.mail.subject.anon",
          "Anonymous comment on {COURSE}: {TITLE}",
          subjectvars,
        );
      }

      let region: RegionModel | undefined;
      if (course.region) {
        region = await Regions.findOneAsync(course.region);
      }
      const emailLogo = region?.custom?.emailLogo;
      const siteName = getSiteName(region);
      const courseLink = Router.url("showCourse", course, {
        query: { campaign },
      });
      const commentLink = Router.url("showCourse", course, {
        query: { select: comment._id, campaign },
      });

      return {
        course: <a href={courseLink}>{course.name}</a>,
        commentLink,
        subject,
        comment,
        commenter,
        commenterLink: Router.url(
          "userprofile",
          { _id: comment.userId, username: commenterName },
          { query: { campaign } },
        ),
        commenterName,
        customSiteUrl: Router.url("home", {}, { query: { campaign } }),
        customSiteName: siteName,
        customEmailLogo: emailLogo,
        unsubscribe: { type: "automatedNotifications", token: unsubToken },
        campaign,
      };
    },
    template: (props) => <Comment {...props} />,
  };
}
