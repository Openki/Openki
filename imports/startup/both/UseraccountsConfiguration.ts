/** The number of days the user (and visitor) remains logged in.  */
export const LoginExpirationInDays = 90;
/** The number of days that the ‘Reset password’ (and ‘Create visitor to user’) token remains valid.  */
export const PasswordResetTokenExpirationInDays = 3;
